from rest_framework import generics
from rest_framework import views
from rest_framework.response import Response

from book.models import Book
from book.serializers import BookSerializer, BookOwnedSerializer


class ListBook(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class DetailBook(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class CustomAPIView(views.APIView):

    def get(self, request, *args, **kwargs):
        queryset = Book.objects.all()
        serializer = BookOwnedSerializer(queryset, many=True)
        return Response(serializer.data)
