from author.serializers import AuthorSerializer
from book.models import Book
from rest_framework import serializers

from api.serializers import UserNameSerializer


class BookSerializer(serializers.ModelSerializer):
    authors = AuthorSerializer(many=True)
    owner = UserNameSerializer(many=True)

    class Meta:
        model = Book
        fields = ('title', 'annotation', 'year', 'pages', 'authors', 'owner')
        extra_kwargs = {
            'id': {'read_only': True}
        }


class BookOwnedSerializer(serializers.ModelSerializer):
    owner = UserNameSerializer(many=True)

    class Meta:
        model = Book
        fields = ('title', 'annotation', 'owner')
        extra_kwargs = {
            'id': {'read_only': True}
        }