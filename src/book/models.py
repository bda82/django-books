from django.db import models
from author.models import Author
from django.contrib.auth.models import User


class Book(models.Model):
    title = models.CharField(max_length=500, default='')
    annotation = models.TextField(default='')
    year = models.SmallIntegerField(default=0)
    pages = models.SmallIntegerField(default=0)
    authors = models.ManyToManyField(Author, related_name="books")
    owner = models.ManyToManyField(User, related_name="books")

    objects = models.Manager()

    def __str__(self):
        return f'<Book> [self.id]: {self.title} {self.year}'
