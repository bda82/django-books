from django.urls import path

from book import views


urlpatterns = [
    path('', views.ListBook.as_view()),
    path('<int:pk>/', views.DetailBook.as_view()),
    path('bbb/', views.CustomAPIView.as_view()),
]