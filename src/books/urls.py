from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls')),
    path('book/', include('book.urls')),
    path('comment/', include('comment.urls')),
    path('author/', include('author.urls')),
]
