from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('url', 'username', 'email',)


class UserNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username',)
