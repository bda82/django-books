from author.models import Author
from rest_framework import serializers


class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Author
        fields = ('first_name', 'last_name', 'surname', 'birth',)
        extra_kwargs = {
            'id': {'read_only': True}
        }
