from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')
    surname = models.CharField(max_length=100, default='')
    birth = models.SmallIntegerField(default=0)

    objects = models.Manager()

    def __str__(self):
        return f'<Author> [self.id]: {self.first_name} {self.last_name}'
