from django.urls import path

from author import views


urlpatterns = [
    path('', views.ListAuthor.as_view()),
    path('<int:pk>/', views.DetailAuthor.as_view()),
]