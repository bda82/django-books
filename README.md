-- test users
curl -H 'Accept: application/json; indent=4' -u admin:admin123! http://127.0.0.1:8000/api/users/

-- test author
curl -H 'Accept: application/json; indent=4' -u admin:admin123! http://127.0.0.1:8000/author/authors/

-- test book
curl -H 'Accept: application/json; indent=4' -u admin:admin123! http://127.0.0.1:8000/book/books/